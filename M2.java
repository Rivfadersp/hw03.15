import java.util.ArrayList;
import java.util.Scanner;

public class M2 {
	
	public static ArrayList<Long> prime(long number){
		
		long k = 2;
		ArrayList<Long> a = new ArrayList<Long>();
		
		while(number/k != 1){
			if(number%k == 0){
				a.add(k);
				number = number/k;
			}
			else{
				if(k==2) k = 3;
				else k = k + 2;
			}
		}
		a.add(number);
		
		return a;
	}
	
	public static double phi(long number){
		ArrayList<Long> a = prime(number);
		boolean f = true;
		double p = 1;

		for(long i=0; i<=number; i++){
			if(a.contains((long)i)) p = p*(1-(double)1/i);
		}

		return number*p;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		long n = s.nextLong();
		ArrayList<Long> a = prime(n);

		for(int i=0; i<a.size(); i++) System.out.print(a.get(i) + " ");

		System.out.println();
		System.out.println((long)phi(n));
	}

}
